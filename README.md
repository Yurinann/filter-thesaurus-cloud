# Filter-Thesaurus-Cloud
A thesaurus for minecraft server (TrChat). 
---
推荐您使用 TrChat 搭配本词库达到最好的效果.
(TrChat: https://github.com/FlickerProjects/TrChat)

使用方法:
```
CLOUD-THESAURUS:
  # 启用
  ENABLE: true
  # 敏感词白名单
  WHITELIST: []
  # 云端词库地址
  URL:
    - 'https://gitee.com/yurinann/Filter-Thesaurus-Cloud/raw/main/database.json'
```
如上，将 CLOUD-THESAURUS.ENABLE 改为 true, CLOUD-THESAURUS.URL 下加入 https://gitee.com/yurinann/Filter-Thesaurus-Cloud/raw/main/database.json 链接 (格式如上), 输入 /trchat reload 即可使用屏蔽功能.

欢迎提交 Pull Requests, 共同维护网络环境.
